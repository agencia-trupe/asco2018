<div class="content center">
    <div class="topico">
        <h1>PÂNCREAS</h1>

        <p class="titulo">
            PRODIGE 24/CCTG PA.6 Unicancer GI trial: estudo fase 3, multicêntrico internacional, com mFOLFIRINOX adjuvante versus gencitabina para pacientes com adenocarcinoma pancreático ductal<sup>4</sup>
        </p>

        <p>
            Estudo fase 3, com 493 pacientes e seguimento mediano de 33,6 meses, cujo desfecho primário era SLP, comparou tratamento adjuvante para câncer de pâncreas (por 6 meses) entre mFOLFIRINOX (A), por 12 ciclos, e gencitabina (B), por 6 ciclos. Foram inclusos pacientes com adenocarcinoma ductal de pâncreas, com ressecção R0 ou R1 que receberam quimioterapia adjuvante em até 12 semanas pós-cirurgia. Foram excluídos pacientes com CA 19,9 ≥ 180 UI/mL avaliados dentro de 21 dias da randomização. Os resultados referentes à sobrevida livre de doença estão representados na Tabela 2.
        </p>

        <p>
            <strong>Tabela 2.</strong> Resultados da sobrevida livre da doença do PRODIGE 24/CCTG PA.6 Unicancer GI trial<sup>4</sup>
        </p>

        <table>
            <thead>
                <th>QT</th>
                <th>SLD Mediana (Meses)</th>
                <th>SLD 3 anos</th>
            </thead>
            <tbody>
                <tr>
                    <th>mFOLFIRINOX</th>
                    <td>21,6 (IC95%:17,7-27,6)</td>
                    <td>39,7% (IC 95%: 32,8-46,6)</td>
                </tr>
                <tr>
                    <th>Gencitabina</th>
                    <td>12,8 (IC95%:11,7-15,2)</td>
                    <td>21,4% (IC 95%: 15,8-27,5)</td>
                </tr>
            </tbody>
        </table>

        <p>
            Em relação à SG mediana, foram 54,4 meses para FOLFIRINOX (IC 95%: 41,8-NA) versus 35 meses para gencitabina (IC95%: 28,7-43,9). Estes resultados são superiores à mediana histórica na adjuvância, e este esquema torna-se padrão para aqueles que apresentam boa performance após tratamento cirúrgico para adenocarcinoma de pâncreas.
        </p>

        <p class="titulo">
            Quimioradioterapia versus cirurgia imediata para câncer pancreático ressecável e borderline ressecável (PREOPANC-1): estudo fase 3, randomizado, controlado e multicêntrico<sup>5</sup>
        </p>

        <p>
            Estudo fase 3 que incluiu 246 pacientes com câncer pancreático ressecáveis. Foram randomizados para cirurgia imediata (braço A) versus quimiorradioterapia pré-operatória com gencitabina (braço B), seguidos por quimioterapia adjuvante. Em análise por intenção de tratar, a SG mediana foi 13,7 (braço A) versus 17,1 meses (braço B), com HR 0,74 e p = 0,074. Em outros desfechos, a quimiorradioterapia pré-operatória também foi melhor, como SLP 7,9 (braço A) versus 9,9 meses (braço B) com HR 0,71 e p = 0,023; ressecção R0 31% versus 65% p < 0,001. Na avaliação do subgrupo com cirurgia R0/R1, a SG foi ainda maior na quimiorradioterapia pré-operatória 16,8 versus 42,2. Não houve diferença entre os eventos diversos grau ≥ 3 entre os dois braços avaliados. Os autores concluem sugerindo que os resultados preliminares mostram benefício da terapia neoadjuvante sobre cirurgia imediata.
        </p>
    </div>
</div>
