<div class="content center">
    <div class="topico">
        <h1>CÓLON</h1>

        <p class="titulo">
            Folfox em primeira linha associado a panitumumabe(pan), seguido de 5-FU/LV associado a pan ou agente único pan como terapia de manutenção para paciente com RAS selvagem em câncer colorretal metastático: estudo VALENTINO<sup>11</sup>
        </p>

        <p>
            Estudo randomizado que investigou se a manutenção com agente único panitumumabe (braço B) era não inferior em relação a 5FU/LV associado a pan (braço A) após 4 meses de FOLFOX-4 associado a panitumumabe.
        </p>

        <p>
            Foram incluídos pacientes com idade > 18 anos, ECOG 0-1, histologicamente confirmados para adenocarcinoma de reto ou cólon metastático com RAS selvagem, sem intenção de ressecção das metástases, sem terapia sistêmica anterior para doença avançada (quimioterapia baseada em oxaliplatina era permitida se o tempo entre o fim do tratamento e a recorrência fosse > 12 meses). O desfecho primário foi a SLP em 10 meses. Em relação à parte estatística, a hipótese nula de inferioridade do braço B em relação ao A seria rejeitada se o limite superior HR estimado fosse < 1,515 com IC 90% em análise unicaudal. Os resultados estão representados nas Tabelas 6 e 7.
        </p>

        <p>
            <strong>Tabela 6.</strong> Taxas de SLP do VALENTINO<sup>11</sup>
        </p>

        <table>
            <thead>
                <tr>
                    <th>SLP</th>
                    <th>SLP em 10 meses</th>
                    <th>SLP mediana</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>Braço A</th>
                    <td>62,8%</td>
                    <td>13,0 meses</td>
                </tr>
                <tr>
                    <th>Braço B</th>
                    <td>52,8%</td>
                    <td>10,2 meses</td>
                </tr>
                <tr>
                    <th>HR</th>
                    <td colspan="2">1,55 (IC 95%: 1,09-2,2; p = 0,011)</td>
                </tr>
            </tbody>
        </table>

        <p>
            <strong>Tabela 7.</strong> Resultados de taxa de resposta global (TRG) e taxa de controle de doença (TCD)<sup>11</sup>
        </p>

        <table>
            <thead>
                <tr>
                    <th></th>
                    <th>TRG</th>
                    <th>TCD</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>Braço A</th>
                    <td>65,8%</td>
                    <td>82,9%</td>
                </tr>
                <tr>
                    <th>Braço B</th>
                    <td>67,0%</td>
                    <td>83,9%</td>
                </tr>
            </tbody>
        </table>

        <p>
            Os autores concluem que, para pacientes com câncer colorretal metastático, RAS selvagem, os quais alcançaram controle da doença após quatro meses de indução com FOLFOX+pan, a manutenção com panitumumabe é provavelmente inferior em relação à combinação de 5-FU/LV+pan em termos de SLP. A eficácia da manutenção de 5-FU/LV associado ao panitumumabe não difere nas principais avaliações de subgrupos.
        </p>
    </div>
</div>
