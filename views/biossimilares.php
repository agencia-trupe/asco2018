<div class="content center">
    <div class="topico">
        <h1>BIOSSIMILARES</h1>

        <p class="titulo">
            Biossimilar trastuzumabe-dkst monoterapia versus trastuzumabe monoterapia após terapia combinada: toxidade, eficácia e imunogenicidade baseadas estudo fase 3 HERITAGE, com seguimento de 48 semanas<sup>13</sup>
        </p>

        <p>
            Este estudo, de fase 3, foi desenhado a partir de diretrizes da Food and Drug Administration (FDA) e European Medicines Agency (EMA) para detectar diferença clinicamente significativa entre o trastuzumabe biossimilar e original, tanto em combinação com taxanos como em monoterapia. Foram incluídas pacientes com câncer de mama HER2+ com recorrência local ou câncer de mama metastático, sem possibilidade de cirurgia curativa e/ou radioterapia, e pacientes tratadas com trastuzumabe ou taxanos em cenário adjuvante foram elegíveis, se doença metastática fosse somente diagnosticada ≥ 1 ano após última dose.
        </p>

        <p>
            A SLP mediana no período de 48 semanas no grupo do trastuzumabe-dkst foi 11,1 (IC 95%: 8,81-11,20) e no trastuzumabe 11,1 (IC 95%: 8,60-11,20), com HR estratificado 0,95 (IC 95%: 0,714-1,251). Os valores de SG não puderam ser estimados neste momento, devido aos dados imaturos. Desta forma, trastuzumabe-dkst é uma opção terapêutica nas indicações em que trastuzumabe é aprovada. Apresentou similar SLP, e os dados de SG estão imaturos no momento, mas com estimativa de serem concluídos no final de 2018.
        </p>

        <p class="titulo">
            Estudo clínico com objetivo de comparar o PF-06439535 (um candidato a biossimilar do bevacizumabe) com bevacizumabe de referência em pacientes com câncer de pulmão de não pequenas células não escamoso avançado<sup>14</sup>
        </p>

        <p>
            Estudo randomizado, duplo-cego e multicêntrico, com objetivo de avaliar a biossimilaridade entre PF-06439535 (A) e bevacizumabe (B). Pacientes (n = 719) foram randomizados 1:1 para PF-06439535 + paclitaxel + carboplatina, por 4-6 ciclos de 21 dias, seguido de manutenção com PF-06439535 ou bevacizumabe + paclitaxel + carboplatina por 4-6 ciclos de 21 dias, seguidos de manutenção com bevacizumabe.
        </p>

        <p>
            Foram inclusos pacientes histológica ou citologicamente predominantemente CPNPC não escamoso, estádio IIIB ou IV ou recorrentes, com pelo menos uma lesão medida por RECIST, sendo excluídos pacientes com metástases em SNC e mutação para EGFR e translocação do EML4-ALK, não sendo permitido tratamento prévio com imunoterapia ou bevacizumabe. A taxa de resposta global na semana 19, confirmada na semana 25, das drogas A e B, foram respectivamente: 45,3% (IC 95%: 40,01-50,57) e 44,6% (IC 95%: 39,4-49,89). Já a SLP (%) e SG mediana (%) em 1 ano para droga A foram respectivamente: 30,8 (IC 95%: 24,6-37,2) e 66,4 (IC 95%: 60,3-71,1); já da droga B foram: 29,3 (IC 95%: 23,5-35,3) e 68,8 (IC 95%: 62,7-74,1). Neste estudo, foi observada similaridade entre as duas drogas em termos de taxa de resposta global. Não foram apresentadas diferenças significativas entre as taxa de SG e SLP nem diferenças significativas no perfil de segurança.
        </p>

        <p class="titulo">
            Comparação da eficácia e segurança do biossimilar filgrastim em estudo controlado e randomizado (PIONEER) e na prática mundial real<sup>15</sup>
        </p>

        <p>
            O estudo PIONEER foi um estudo fase III para câncer de mama em que se randomizou 1:1 a droga de referência versus filgrastim, na qual o endpoint primário foi a duração de severa neutropenia no ciclo 1. Já o MONITOR-GCSF foi um estudo prospectivo, observacional, aberto e fármaco-epidemiológico, que avaliou segurança no contexto pós-aprovação do biossimilar do filgrastim.
        </p>

        <p>
            O objetivo do estudo em questão foi avaliar os resultados do PIONNER e do MONITOR-GCSF, cujos resultados dos níveis de eficácia estão apresentados na Tabela 8.
        </p>

        <p>
            <strong>Tabela 8.</strong> Resultados dos níveis de eficácia do PIONNER e MONITOR-GCSF<sup>15</sup>
        </p>

        <table>
            <thead>
                <tr>
                    <th>Eventos, n (%)</th>
                    <th>PIONEER (n = 117)</th>
                    <th>MONITOR-GCSF (n = 466)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>NF em qualquer ciclo</th>
                    <td>11 (5,1)</td>
                    <td>29 (6,2)</td>
                </tr>
                <tr>
                    <th>Severa neutropenia</th>
                    <td>159 (74,3)</td>
                    <td>91 (19,5)</td>
                </tr>
                <tr>
                    <th>Temp > 38,5</th>
                    <td>12 (5,6)</td>
                    <td>16 (3,4)</td>
                </tr>
                <tr>
                    <th>Infecção</th>
                    <td>17 (7,9)</td>
                    <td>72 (15,5)</td>
                </tr>
            </tbody>
        </table>

        <p>
            Quando avaliada a duração (em dias) sem neutropenia no PIONEER no ciclo 1, foi 1,17±1,11 (biossimiar) e 1,2 ± 1,02 (referência). O autor concluiu que filgrastim biosimilar previne neutropenia febril tanto em estudos controlados como na vida real de pacientes com câncer de mama recebendo (neo)adjuvante quimioterapia.
        </p>

        <p class="titulo">
            Abordagem de tratamento para pacientes com Linfoma não-Hodgkin desde primeiro biossimilar de rituximabe aprovado na Europa<sup>16</sup>
        </p>

        <p>
            O objetivo deste estudo foi avaliar o padrão de prescrição para os pacientes com Linfoma não-Hodgkin (LNH), para avaliar potenciais mudanças entre mAB de referência e biossimilar. Este foi um estudo multicêntrico cujos médicos responsáveis pela prescrição de drogas anticâncer para pacientes com LNH preenchiam um formulário online médico (o estudo teve participação de 97 médicos e 640 formulários médicos preenchidos).
        </p>

        <p>
            Comparando os pacientes tratados com rituximabe de referência com aqueles que foram tratados com rituximabe biossimilar, observou-se maior probabilidade de LNH indolente do que agressivo (70% versus 52%) e ter linfoma folicular como subtipo para LNH (56% versus 35%).
        </p>

        <p>
            Os autores concluíram que, no caso de pacientes mais saudáveis, com histologia indolente e LNH com subtipo folicular, são as situações quando são mais prescritos o rituximabe biossimilar. Médicos que começam a prescrever rituximabe biossimilar parecem adotar a estratégia mais conservadora, iniciando o biossimilar em situações quando os potenciais efeitos colaterais seriam menos debilitantes, como naqueles com melhor saúde ou com melhor prognóstico. Além disso, com nível de experiência aumentando com a droga ao longo do tempo, os médicos poderão começar a prescrever biossimilar em detrimento à droga de referência, mas no momento ainda não faz parte da prática clínica diária.
        </p>
    </div>
</div>
