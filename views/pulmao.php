<div class="content center">
    <div class="topico">
        <h1>PULMÃO</h1>

        <p class="titulo">
            KEYNOTE-042: pembrolizumabe versus quimioterapia baseada em platina como terapia de primeira linha para câncer de pulmão de não pequenas células avançado/metastático com PD-L1≥1% – fase 3<sup>6</sup>
        </p>

        <p>
            Estudo anterior (KEYNOTE 024) demonstrava que pembrolizumabe (P) em monoterapia, em relação à quimioterapia (QT) e baseado em platina para pacientes com câncer de pulmão de não pequenas células (CPNPC) metastático com PD-L1 ≥ 50% e sem alteração EGFR e ALK, era superior em SLP e SG. O KEYNOTE 0-42, apresentado em sessão plenária, investigou o papel do pembrolizumabe como primeira linha para pacientes com PD-L1 ≥ 1%. Foram inclusos pacientes CPNPC localmente avançados ou metastáticos não tratados, PD-L1 ≥ 1%, sem alteração ALK ou EGFR, ECOG 0-1. Os desfechos primários foram SG para PD-L1 ≥ 50%, PD-L1 ≥ 20%, PD-L1 ≥ 1%. Pacientes foram randomizados 1:1 para pembrolizumabe por até 35 ciclos ou quimioterapia baseado em platina (carboplatina + paclitaxel ou carboplatina + pemetrexede) por até 6 ciclos. Os resultados de SG estão apresentados na Tabela 3.
        </p>

        <p>
            <strong>Tabela 3.</strong> Resultados de SG KEYNOTE-042<sup>6</sup>
        </p>






















        <table>
            <tbody>
                <tr>
                    <th>PD-L1≥1%</th>
                    <th>SG (meses)</th>
                </tr>
                <tr>
                    <td>Pembro</td>
                    <td>16,7</td>
                </tr>
                <tr>
                    <td>QT</td>
                    <td>12,1</td>
                </tr>
                <tr>
                    <td colspan="2">HR 0,81(IC95%: 0,71-0,93)</td>
                </tr>
                <tr>
                    <th>PD-L1≥20%</th>
                    <th>SG (meses)</th>
                </tr>
                <tr>
                    <td>Pembro</td>
                    <td>17,7</td>
                </tr>
                <tr>
                    <td>QT</td>
                    <td>13</td>
                </tr>
                <tr>
                    <td colspan="2">HR 0,77(IC95%: 0,64-0,92)</td>
                </tr>
                <tr>
                    <th>PD-L1≥50%</th>
                    <th>SG (meses)</th>
                </tr>
                <tr>
                    <td>Pembro</td>
                    <td>20</td>
                </tr>
                <tr>
                    <td>QT</td>
                    <td>12,2</td>
                </tr>
                <tr>
                    <td colspan="2">HR 0,69(IC95%:0,56-0,85)</td>
                </tr>
            </tbody>
        </table>

        <p>
            Em relação à SLP, foram obtidos os seguintes resultados:<br>
            PD-L1 ≥ 50%: 7,1 versus 6,4 meses HR 0,81 (IC 95%: 0,67-0,99; p = 0,0170).<br>
            PD-L1 ≥ 20%: 6,2 versus 6,6 meses HR 0,94 (IC95%: 0,80-1,11).<br>
            PD-L1 ≥ 1%: 5,4 versus 6,5 meses HR 1,07 (IC95%: 0,94-1,21).
        </p>

        <p>
            Pembrolizumabe significantemente aumentou a SG em quimioterapia baseada em platina como primeira linha para CPNPC avançado/metastático com PD-L1 ≥ 50%, ≥ 20% e ≥ 1%. Entretanto, como em estudos anteriores, a magnitude do benefício foi maior quanto maior os níveis de PD-L1. Na análise exploratória de PD-L1 1-49%, HR foi de 0,92 (0,77-1,11).
        </p>


        <p class="titulo">
            Estudo Fase 3 em que se comparou bevacizumabe+erlotinibe ao erlotinibe em pacientes com CPNPC com presença de mutações ativadoras do EGFR: NEJ 026<sup>7</sup>
        </p>

        <p>
            Estudo fase 3, randomizado, com 214 pacientes entre bevacizumabe + erlotinibe (BE) ou erlotinibe monoterapia (E).
        </p>

        <p>
            Foram inclusos pacientes sem quimioterapia prévia, CPNPC não escamoso, estádio IIIB/IV ou recorrência pós-operatória, com presença de mutação ativadora de EGFR (deleção éxon 19, éxon 21 L858R), sendo permitida metástase assintomática em sistema nervoso central e excluídos pacientes com mutação T790M. O desfecho primário era SLP. A duração do seguimento foi 12,4 meses.
        </p>

        <p>
            Os resultados encontrados foram: SLP mediana com HR 0,605 (IC 95%: 0,417-0,877; p = 0,01573) 16,9 meses (BE) versus 13,3 meses (E). A avaliação por subtipo de mutação EGFR está apresentada na <strong>Tabela 4</strong>.
        </p>

        <p>
            <strong>Tabela 4.</strong> Resultados de SLP de acordo com mutação EGFR do NEJ 026<sup>7</sup>
        </p>

        <table>
            <thead>
                <th>Deleção éxon 19</th>
                <th>SLP (meses)</th>
            </thead>
            <tbody>
                <tr>
                    <td>BE</td>
                    <td>16,6</td>
                </tr>
                <tr>
                    <td>E</td>
                    <td>12,4</td>
                </tr>
                <tr>
                    <td colspan="2">HR 0,69 (IC95%: 0,41-1,16)</td>
                </tr>
            </tbody>
        </table>

        <table>
            <thead>
                <tr>
                    <th>Éxon 21 L858R</th>
                    <th>SLP (meses)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>BE</td>
                    <td>17,4</td>
                </tr>
                <tr>
                    <td>E</td>
                    <td>13,7</td>
                </tr>
                <tr>
                    <td colspan="2">HR 0,57(IC95%: 0,33-0,97)</td>
                </tr>
            </tbody>
        </table>

        <p>
            Ao avaliar a taxa de resposta global, observou-se os seguintes resultados: 72,3% (BE) versus 66,1% (E).
        </p>

        <p>
            Em relação à descontinuação do tratamento, o erlotinibe foi suspenso em 21 (18,8%) no grupo BE versus 17 (15,2%) no Grupo E. Já o bevacizumabe foi descontinuado em 33 (29,5%) no grupo BE. Foram observados eventos adversos  3 em 63 (56,3%) no grupo BE e 43 (37,7%) no grupo E.
        </p>

        <p>
            Os autores concluem que NEJ026 é o primeiro estudo fase 3 que investiga bevacizumabe + erlotinibe em pacientes com CPNPC não tratadas e com EGFR mutado. A combinação BE demonstra significante aumento de SLP e é bem tolerada, sendo que este regime se apresenta como novo tratamento padrão para pacientes com CPNPC com EGFR mutado.
        </p>
    </div>
</div>
