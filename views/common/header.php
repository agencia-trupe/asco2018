<header class="center">
    <div class="asco"></div>

    <div class="menu">
        <div class="abertura">
            <p>
                Considerado um dos Congressos mais importantes de Oncologia, o <strong>ASCO Annual Meeting 2018</strong> reuniu na cidade de Chicago (Illinois), no período de 1º a 05 de junho, diversos especialistas que discutiram inovações, controvérsias e tendências no tratamento do câncer e no cuidado do paciente. A seguir os tópicos de maior destaque:
            </p>
        </div>

        <div class="autores">
            <p>
                <strong>Dr. Ariel Galapo Kann</strong><br>
                CRM-SP 112.002<br>
                <small>Coordenador da Oncologia Clínica do Centro Especializado em Oncologia do Hospital Alemão Oswaldo Cruz</small>
            </p>
            <p>
                <strong>Dr. Denis Sakamoto Shimba</strong><br>
                CRM-SP 143.538<br>
                <small>Oncologista Clínico do Centro Especializado em Oncologia do Hospital Alemão Oswaldo Cruz</small>
            </p>
        </div>

        <nav>
            <a href="/mama" <?php if($slug == 'mama') echo 'class="active"'; ?>>MAMA</a>
            <a href="/pancreas" <?php if($slug == 'pancreas') echo 'class="active"'; ?>>PÂNCREAS</a>
            <a href="/pulmao" <?php if($slug == 'pulmao') echo 'class="active"'; ?>>PULMÃO</a>
            <a href="/geniturinario" <?php if($slug == 'geniturinario') echo 'class="active"'; ?>>GENITURINÁRIO</a>
            <a href="/colon" <?php if($slug == 'colon') echo 'class="active"'; ?>>CÓLON</a>
            <a href="/ginecologico" <?php if($slug == 'ginecologico') echo 'class="active"'; ?>>GINECOLÓGICO</a>
            <a href="/biossimilares" <?php if($slug == 'biossimilares') echo 'class="active"'; ?>>BIOSSIMILARES</a>
        </nav>
    </div>
</header>
