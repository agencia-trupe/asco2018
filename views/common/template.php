<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hot Topics ASCO Annual meeting 2018</title>
    <meta name="author" content="Trupe Agência Criativa">
    <meta name="copyright" content="<?php echo date('Y'); ?> Trupe Agência Criativa">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="stylesheet" href="/assets/main.css">
</head>
<body>
<?php require_once __DIR__.'/header.php'; ?>
<?php require_once $view; ?>
<?php require_once __DIR__.'/footer.php'; ?>
</body>
</html>
