<footer>
    <div class="center">
        <div class="informacoes">
            <div class="marcas">
                <img src="/assets/sandoz.png" alt="">
                <img src="/assets/segmento-farma.png" alt="">
            </div>

            <p class="aviso">
                O conteúdo desta obra é de inteira responsabilidade de seu(s) autor(es).<br>
                Produzido por Segmento Farma Editores Ltda., sob encomenda de Sandoz, em setembro de 2018.<br>
                MATERIAL DE DISTRIBUIÇÃO EXCLUSIVA À CLASSE MÉDICA.
            </p>

            <p class="contato">
                SAC 0800 4009192<br>
                <a href="mailto:sac.sandoz@sandoz.com">sac.sandoz@sandoz.com</a>
            </p>

            <p class="endereco">
                Sandoz do Brasil Indústria Farmacêutica<br>
                Rodovia Celso Garcia Cid - PR 445 km 87<br>
                CEP: 86183-600 - Cambé / PR – Brasil
            </p>
        </div>
    </div>
</footer>
