<div class="content center">
    <div class="topico">
        <h1>MAMA</h1>

        <p class="titulo">
            Estudo fase 3 de terapia quimioendócrina versus terapia endócrina exclusiva em pacientes com câncer de mama receptor hormonal positivo, HER2 negativo, linfonodo negativo e com prognóstico intermediário pelo escore de recorrência com expressão de 21 genes (TAILORx)<sup>1</sup>
        </p>

        <p>
            TAILORx é um estudo de fase 3, apresentado em plenária, que tinha como objetivo avaliar a real necessidade de quimioterapia em pacientes com risco intermediário (Oncotype DX® escore 11-25). Comparou-se a realização de hormonioterapia isolada (braço A) versus hormonioterapia e quimioterapia (braço B). Foram inclusas mulheres com câncer de mama inicial, HER2 negativo, receptor hormonal positivo, sem acometimento de linfonodos axilares e que apresentavam risco intermediário no teste Oncotype DX®. Neste estudo, foram inclusas 10.273 mulheres entre 18 a 75 anos, com  tamanho  tumoral  1,1-5,0  cm  (ou 0,6 a 1 cm em grau intermediário-alto). O seguimento mediano foi de 7,5  anos.  As  taxas  de  sobrevida  livre de doença foram 83,3%  (braço  A)  x  84,3%  (B),  taxas de recorrência 94,5% (A) x 95% (B) e sobrevida global (SG) 93,9% (A) x 93,8 % (B). Em conclusão, a terapia endócrina isolada não foi inferior à quimioendócrina em pacientes com risco intermediário (11-25). Há sugestão de algum benefício da adição da quimioterapia para mulheres com menos de 50 anos com índice de recorrência 21-25, devendo ser discutido caso a caso.
        </p>

        <p class="titulo">
            PERSEPHONE: 6 versus 12 meses de trastuzumabe adjuvante em pacientes com câncer de mama inicial HER2 positivo: estudo de fase 3 de não inferioridade – resultados de sobrevida livre de doença após seguimento de 4 anos<sup>2</sup>
        </p>

        <p>
            Estudo de não inferioridade, fase 3, em que se avaliou, em amostra de 4.009 pacientes com seguimento mediano de 4,5 anos se 6 meses de trastuzumabe seria similar ao tratamento padrão com 12 meses. Foram incluídas pacientes HER2+ com câncer de mama inicial e clara indicação de quimioterapia, sem evidência de doença metastática e com conhecido status do receptor hormonal. As pacientes realizaram quimioterapia baseados em antracíclicos, taxanos, antracíclicos + taxanos ou CMF.
        </p>

        <p>
            O tratamento completo com trastuzumabe foi realizado em 94% dos pacientes, sendo que 86% dos pacientes tinham correto número de doses. Resultados: sobrevida livre de doença em 4 anos: 89,4% (6 meses) e 89,8% (12 meses); HR = 1,07 (IC 90%: 0,93-1,24); p = 0,01. SG em 4 anos: 93,8% (6 meses) e 94,8% (12 meses); HR 1,14 (IC 90%: 0,95-1,37); p = 0,0006. Em relação aos efeitos colaterais, 20% dos pacientes sequenciais apresentaram toxidade  G3/G4  (23%  para  12  meses e 18% para 6 meses com p = 0,04). Conclui-se que 6 meses de trastuzumabe é não inferior do que 12 meses. Além disso, ao se comparar os dois esquemas, 6 meses reduzem toxidades cardíacas e outras toxidades, além dos custos para pacientes e sistema de saúde.
        </p>

        <p class="titulo">
            Ribociclibe + fulvestranto em mulheres pós-menopausa com receptor hormonal positivo e HER2 negativo em câncer de mama avançado: resultados do MONALEESA-3<sup>3</sup>
        </p>

        <p>
            Estudo fase III, placebo-controlado e com seguimento mediano de 20,4 meses, no qual foram avaliados 726 pacientes e randomizados entre o tratamento com ribociclibe (R) + fulvestranto (F) (n = 484) ou placebo (P) + fulvestranto (F) (n = 242). O desfecho primário foi sobrevida livre de progressão (SLP). Foram inclusas mulheres em pós-menopausa e homens que apresentavam ≥ 1 lesões mensuráveis (RECIST 1.1) ou ≥ 1 lesões ósseas predominantemente líticas, ECOG ≤ 1, com até uma linha de terapia endócrina anterior, e foram excluídos aqueles com tratamento quimioterápico anterior para câncer de mama avançado ou fulvestranto ou qualquer inibidor de CDK4/6, bem como pacientes com câncer de mama inflamatório. As taxas de SLP estão apresentadas na Tabela 1 (IC 95%).
        </p>

        <p>
            <strong>Tabela 1.</strong> Taxas de SLP do MONALEESA-33
        </p>

        <table>
            <thead>
                <th>SLP</th>
                <th>R+F</th>
                <th>P+F</th>
            </thead>
            <tbody>
                <tr>
                    <th>SLP mediana</th>
                    <td>20,5</td>
                    <td>12,8</td>
                </tr>
                <tr>
                    <th>HR</th>
                    <td colspan="2">0,593 (0,480-0,732)</td>
                </tr>
                <tr>
                    <th>p unicaudal</th>
                    <td colspan="2">0,0000041</td>
                </tr>
            </tbody>
        </table>

        <p>
            Este estudo demonstrou ganho estatístico significante em termos de SLP para grupo ribociclibe + fulvestranto. Este ganho foi observado tanto em primeira linha (HR 0,577; IC95%: 0,415-0,802) quanto em segunda linha  (HR  0,565;  IC95%:  0,428-0,744).  Desta  forma, a combinação R+P torna-se interessante alternativa para o tratamento, particularmente em casos de câncer de mama avançado de novo e pacientes com recorrência > 12 meses, após término da terapia endócrina (neo) adjuvante.
        </p>
    </div>
</div>
