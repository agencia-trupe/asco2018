<div class="content center">
    <div class="topico">
        <h1>GINECOLÓGICO</h1>

        <p class="titulo">
            Estudo de fase 3 randomizado e controlado, de citorredução secundária cirúrgica (CSC), seguido de quimioterapia combinada e baseado em platina (QBP) com ou sem bevacizumabe (B): um estudo do GOG<sup>12</sup>
        </p>

        <p>
            Estudo randomizado com seguimento de 34,6 meses, cujo desfecho primário era determinar (i) se adição de bevacizumabe a paclitaxel e carboplatina seguida da manutenção com bevacizumabe aumentaria SG em relação à carboplatina + paclitaxel para pacientes com câncer de ovário platinossensível recorrente e (ii) se citorredução secundária seguida de quimioterapia aumentaria a SG de câncer de ovário platinossensível recorrente. Foram inclusas pacientes com câncer de ovário com resposta completa previamente, intervalo de platina ≥ 6 meses e recorrência clinicamente evidente ou confirmada por biópsia. Ao avaliar cirurgia versus não cirurgia, foram apresentados os seguintes resultados: SG HR (ITT): 1,28 (0,92-1,78) e SLP HR = 0,88 (0,70-1,11). Ao avaliar os resultados da adição do bevacizumabe, foram encontrados: SG HR = 0,829 (0,68-1,005) e p = 0,056, e HR 0,823 (0,68-0,996) e p = 0,0447.
        </p>

        <p>
            A citorredução secundária não foi associada ao aumento de SG e SLP ao comparar com a não realização de cirurgia.
        </p>
    </div>
</div>
