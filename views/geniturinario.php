<div class="content center">
    <div class="topico">
        <h1>GENITURINÁRIO</h1>

        <p class="titulo">
            CARMENA: nefrectomia citorredutora seguida por sunitinibe versus sunitinibe monodroga em carcinoma renal metastático – resultado do estudo de fase 3 de não inferioridade<sup>8</sup>
        </p>

        <p>
            Estudo prospectivo, multicêntrico e aberto, cujo desfecho primário era SG. Foram inclusos carcinomas de células renais (CCR) do tipo células claras, ECOG 0-1, passíveis para nefrectomia, elegível para sunitinibe e sem terapia anterior para CCR. Pacientes (n = 450) foram randomizados 1:1 para nefrectomia, seguida de sunitinibe (braço A) ou sunitinibe (braço B), com seguimento mediano de 50,9 meses. Os resultados estão apresentados na Tabela 5.
        </p>

        <p>
            <strong>Tabela 5.</strong> SG (intenção de tratar) do CARMENA<sup>8</sup>
        </p>

        <table>
            <thead>
                <tr>
                    <th>SG mediana<br>(meses) IC 95%</th>
                    <th>Braço A</th>
                    <th>Braço B</th>
                    <th>HR IC 95%</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>Global</th>
                    <td>13,9 (11,8-18,3)</td>
                    <td>18,4 (14,7-23)</td>
                    <td>0,89 (0,71-1,1)</td>
                </tr>
                <tr>
                    <th>MSKCC- Intermediário</th>
                    <td>19 (12-28)</td>
                    <td>23,4 (17-32)</td>
                    <td>0,92 (0,6-1,24)</td>
                </tr>
                <tr>
                    <th>MSKCC- Alto</th>
                    <td>10,2 (9-14)</td>
                    <td>13,3 (9-17)</td>
                    <td>0,86 (0,62-1,7)</td>
                </tr>
            </tbody>
        </table>

        <p style="font-size:.8em;font-style:italic;margin-top:-1em">
            SLP (intenção de tratar); IC 95%: braço A 7,2 (6,5-8,5) versus braço B 8,3 (6,2-9,9); HR 0,82 (0,67-1,0).
        </p>

        <p>
            Os autores concluem que sunitinibe é não inferior à nefrectomia citorredutora seguida de sunitinibe para SG, tanto em risco intermediário quanto alto. O benefício clínico foi maior no braço com sunitinibe isolado.
        </p>

        <p class="titulo">
            Olaparibe combinado com abiraterona para pacientes com câncer de próstata metastático de castração resistente (mCPCR): estudo fase II randomizado<sup>9</sup>
        </p>

        <p>
            Baseado na hipótese de sinergismo entre abiraterona e olaparibe, surge este estudo fase II, que randomiza 1:1 para olaparibe + abiraterona (braço A) ou placebo + abiraterona (braço B). O desfecho primário foi SLP radiológica, sendo inclusos pacientes com mCPCR, sem tratamento prévio com docetaxel para mCPCR, ≤ 2 linhas anteriores de quimioterapia e sem uso prévio de agentes anti-hormonal de segunda geração.
        </p>

        <p>
            A SLP (meses) mediana no braço A foi 13,8 versus braço B (8,2), com HR 0,65 (IC 95%: 0,44-0,97; p = 0,034). A SG mediana foi de 22,7 (braço A) versus 20,9 (braço B) (HR 0,91; IC 95%: 0,60-1;38). Os autores concluem que a associação olaparibe + abiraterona aumentou significativamente o benefício de SLP para pacientes que tinham recebido previamente docetaxel, comparada com abiraterona, e o benefício foi independente do status de reparo de recombinação homóloga. Entretanto, a combinação foi associada com o aumento de incidência de eventos adversos (EA). EA ≥ 3 acometeram 54% versus 28%, respectivamente.
        </p>

        <p class="titulo">
            PROPHECY Trial: estudo prospectivo multicêntrico de detecção de células tumorais AR-V7 de homens com câncer de próstata com castração resistente e recebendo abiraterona ou enzalutamida<sup>10</sup>
        </p>

        <p>
            Estudo prospectivo, que teve como objetivo validar a presença do AR-V7 nas células circulantes tumorais como biomarcador preditivo em CPCR. Foram incluídos pacientes com adenocarcinoma de próstata, sem características de pequenas células, com níveis de testosterona de castração (≤ 50 ng/dL), com terapia planejada com abiraterona ou enzalutamida e que apresentassem duas ou mais características de alto risco (anemia, elevação de fosfatase alcalina, elevação da DHL sérica, terapia anterior com enzalutamida ou abiraterona, presença de metástase visceral nas imagens, significante quadro álgico com necessidade de opioide, CTC ≥ 5, PSADT < 3 meses).
        </p>

        <p>
            Não houve diferença significativa de SLP e SG entre enzalutamida e abiraterona; a SLP foi 5,8 meses e SG foi de 20,3 nos CPCR de alto risco. Homens com CTCs (+) para AR-V7 eram mais prováveis de ter ≥ 5 células CTCs do que AR-V7(-), tanto no teste RNA CTC AR-V7 Johns Hopkins (71% versus 31%) quanto no nuclear CTC AR-V7 EPIC (63% versus 39%). A concordância para AR-V7 teste foi de 82%, e a SG mediana no teste Johns Hopkins foi de 10,8 (AR-V7+) versus 27,2 meses (AR-V7-); SLP, respectivamente, 3,1 meses versus 6,9 meses. Já no teste EPIC, a SG foi 8,4 meses (AR-V7+) versus 20,3 meses (AR-V7-), e a SLP, respectivamente, 3,1 meses versus 6,9 meses.
        </p>

        <p>
            Este estudo validou AR-V7 CTC como um biomarcador preditivo negativo da terapia inibitória de receptor androgênico. Tanto o teste Epic como Johns Hopkins foram preditivos de má resposta em homens com CPCR, independentemente da linha terapêutica. Homens com AR-V7 CTC+ têm baixa probabilidade de benefício com abiraterona ou enzalutamida (0-12% de chance).
        </p>
    </div>
</div>
