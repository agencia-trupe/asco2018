<?php

$slug = trim($_SERVER['REQUEST_URI'], '/') ?: 'mama';

$view = __DIR__.'/views/'.$slug.'.php';
if (! file_exists($view)) $view = __DIR__.'/views/404.php';

require __DIR__.'/views/common/template.php';
